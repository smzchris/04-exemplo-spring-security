package br.com.itau.autenticacao.helpers;

import java.util.Optional;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JwtHelper {
	private static final String secret = "9ndv9un3";

	public static Optional<String> gerar(long id) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(secret);
			String token = JWT.create().withClaim("idUsuario", id).sign(algorithm);
			
			return Optional.of(token);
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	public static Optional<Integer> verificar(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(secret);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			Integer id = jwt.getClaim("idUsuario").asInt();
			
			return Optional.of(id);
		} catch (Exception e) {
			return Optional.empty();
		}
		
	}

}
